# Eonrust

Use rust-elements to create p2sh transactions, build a library with cpython so that it can be used in Flask projects.

The script we use to create escrows for Discreet Log Contracts is the following: `or(pka, and(pkb, older(locktime)))`

 - pka is the party that can spend if the Oracle confirms the outcome (see Eonbasics, the private key that can spend is built with party A's private key and a message signed by the oracle)
 - pkb is the party that can unlock the money after a locktime has passed (the Oracle doesn't sign the outcome expected by party A).

## Getting started

To build on MacOS you may need to create `.cargo/config`

```
[target.x86_64-apple-darwin] //or aarch64-apple-darwin for M1
rustflags = [
  "-C", "link-arg=-undefined",
  "-C", "link-arg=dynamic_lookup",
]
```

To demo some outputs you can run:
```
cargo build --release
./target/release/eonrust
```

In the target build folder you'll also find `libeonrust.dlyb/.so/.dll` that can be imported into Python projects, to do so you have to rename it to: `eonrust.so` if running on Linux systems. In Eonbasics we provide a courtesy compiled vesion for linux ltm, if you run other architectures you have to recompile it.
