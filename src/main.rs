fn main() {

    let address = eonrust::_create_p2sh_address("03e6e9385ce0203306c819540b21947ba3f47b904cb93fc8d626acc35857f34951", 
        "025b0513b7055b35afa51f5921fcfa84a8480332a7f52b404584ef12e43881e5db", "3");
    println!("{}", address);

    let tx = eonrust::_create_spending_tx(
        "cTcUoa4FbYCRYFXUnikAW9LYexM9QovQcgd9z8CoV1pM6fuJh48W",
        "db087b014e5bf5e53ff814bf9ad291e6f010035fcfbb8083033e1e1092e1a632",
        "2",
        "10000000",
        "1000",
        "03e6e9385ce0203306c819540b21947ba3f47b904cb93fc8d626acc35857f34951", 
        "025b0513b7055b35afa51f5921fcfa84a8480332a7f52b404584ef12e43881e5db", 
        "3"
    );
    println!("{}", tx);

}