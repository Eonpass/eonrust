#[macro_use]
extern crate cpython;
use cpython::{Python, PyResult, py_module_initializer, py_fn};

extern crate rand;
extern crate bitcoin;
extern crate elements;
extern crate secp256k1_zkp as secp256k1_stdalone;

use crate::elements::secp256k1_zkp::bitcoin_hashes::hex::ToHex;
use crate::elements::pset::serialize::Serialize;
use std::{num::ParseIntError, str::FromStr};


//use rand::SeedableRng; //TODO: this is useful for commitments I guess, not used so far

use elements::{AssetId, confidential, secp256k1_zkp, TxOut, Txid, Transaction};
use elements::hashes::hex::FromHex;

/// Creates a p2sh address for our desired script: or(pka, and(pkb, older(locktime)))
///
/// # Arguments
///
/// * `_py` - cpython binding
/// * `pka` - BIP representation of a public key, string
/// * `pkb` - BIP representation of a public key, string
/// * `locktime` - string representing an int, the number of blocks after which keyB can spend, string
fn create_p2sh_address(_py: Python, pka: &str, pkb: &str, locktime: &str) -> PyResult<String> {

    let address = _create_p2sh_address(pka, pkb, locktime);

    Ok(address)
}

/// Create and sign a tx spending from our desired script: or(pka, and(pkb, older(locktime))).
/// We assume there is no change and enough coins for the fees inside the outpoint we will be spending
///
/// # Arguments
///
/// * `_py` - cpython binding
/// * `privka` - WIF representation of party A's private key, string
/// * `txid` - transaction id of the outpoint where we put the coins that will be input for this transaction, string
/// * `voutn` - index of the vout of the outpoit where we put the coins that will be input for this transaction, string
/// * `pka` - BIP representation of a public key, string
/// * `pkb` - BIP representation of a public key, string
/// * `locktime` - string representing an int, the number of blocks after which keyB can spend, string
fn create_spending_tx(_py: Python, privka: &str, txid: &str, voutn: &str,  value: &str, fee: &str, pka: &str, pkb: &str, locktime: &str) -> PyResult<String> {

    let rawtx = _create_spending_tx(privka, txid, voutn, value, fee, pka, pkb, locktime);

    Ok(rawtx)
}

py_module_initializer!(eonrust, |py, m | {
    m.add(py, "__doc__", "Create a P2SH address for the script or(pka, and(pkb, older(locktime)))")?;
    m.add(py, "create_p2sh_address", py_fn!(py, create_p2sh_address(pka: &str, pkb: &str, locktime: &str)))?;
    m.add(py, "create_spending_tx", py_fn!(py, create_spending_tx(privka: &str, txid: &str, voutn: &str,  value: &str, fee: &str, pka: &str, pkb: &str, locktime: &str)))?;
    Ok(())
});

/// Creates a p2sh address for our desired script: or(pka, and(pkb, older(locktime)))
///
/// # Arguments
///
/// * `pka` - BIP representation of a public key, string
/// * `pkb` - BIP representation of a public key, string
/// * `locktime` - string representing an int, the number of blocks after which keyB can spend, string
pub fn _create_p2sh_address(pka: &str, pkb: &str, locktime: &str) -> String {

    let locktime_int = locktime.parse::<i64>().unwrap();

    let a_bytes: [u8; 33] =  vec_to_array(decode_hex(pka).unwrap());
    let a = elements::bitcoin::PublicKey::from_slice(&a_bytes).ok().unwrap();

    let b_bytes: [u8; 33] = vec_to_array(decode_hex(pkb).unwrap());
    let b = elements::bitcoin::PublicKey::from_slice(&b_bytes).ok().unwrap();

    let spk = _create_redeem_script(&a, &b, locktime_int);

    let address = format!("{}", elements::Address::p2sh(&spk, None, &elements::AddressParams::ELEMENTS));
    return address
}

/// Get the txid and voutn of the escrow tx, a private key that can spend from the script and return the signed transaction hex.
/// At the moment we expect top have key A, the one that can sign even before the locktime expires, TODO: is there any difference if we have key B?
///
/// # Arguments
///
/// * `priva` - WIF representation of a private key, string
/// * `txid` - txid of the output to spend, string
/// * `voutn` - string representing an int, the number of the output to spend, string
/// * `value` - string representing an int, value of the spendable output in satoshis, string
/// * `fee` - string representing an int, fee for the transaction in satoshis, string
/// * `pka` - BIP representation of a public key, string
/// * `pkb` - BIP representation of a public key, string
/// * `locktime` - string representing an int, the number of blocks after which keyB can spend, string
pub fn _create_spending_tx(signing_key: &str, txid: &str, voutn: &str, value: &str, fee: &str, pka: &str, pkb: &str, locktime: &str) -> String {

    //prepare the spendable output:
    let voutn_int = voutn.parse::<u32>().unwrap();    
    let spendable_output =  elements::OutPoint::new(
        Txid::from_str(txid).unwrap(),
        voutn_int
    );

    //prepare the redeem_script by rebuilding the script
    let locktime_int = locktime.parse::<i64>().unwrap();
    let value_int = value.parse::<u64>().unwrap();
    let fee_int = fee.parse::<u64>().unwrap();
    if fee_int >= value_int {
        panic!("fee greater than value!");
    }
    let a_bytes: [u8; 33] =  vec_to_array(decode_hex(pka).unwrap());
    let a = elements::bitcoin::PublicKey::from_slice(&a_bytes).ok().unwrap();
    let b_bytes: [u8; 33] = vec_to_array(decode_hex(pkb).unwrap());
    let b = elements::bitcoin::PublicKey::from_slice(&b_bytes).ok().unwrap();
    let redeem_script = _create_redeem_script(&a, &b, locktime_int);
    let redeem_script_bytes = redeem_script.as_bytes();

    //understand if this branch A (winner with oracle signed message) or B (default payment)
    let curve = bitcoin::secp256k1::SECP256K1;
    let bitcoin_priv_key = bitcoin::PrivateKey::from_wif(&signing_key).unwrap();
    let signing_pubkey = bitcoin_priv_key.public_key(curve);
    let mut is_a_signing = 0;
    if signing_pubkey.to_bytes()==a.to_bytes(){
        is_a_signing = 1;
    }
    let version = if is_a_signing==1 {1} else {2};

    //prepare the scriptsig:
    // 1) create a fake Tx with scriptpub_key as scriptsig and use it to create the sighash
    let partial_tx = _create_tx_to_sign(redeem_script.clone(), locktime_int, spendable_output.clone(), value_int, fee_int, a, version).clone();
    let sighash = elements::sighash::SigHashCache::new(&partial_tx).legacy_sighash(
            0, //our tx only has 1 input, no multiple inputs for now
            &redeem_script,
            elements::SigHashType::All,
    );

    // 2) sign sig_hash and create the script_sig    
    let priv_bytes: [u8; 32] = vec_to_array(bitcoin_priv_key.to_bytes());
    let secret_priv_key = bitcoin::secp256k1::SecretKey::from_slice(&priv_bytes).unwrap(); 
    let redeem_message = bitcoin::secp256k1::Message::from_slice(&sighash[..]).unwrap(); 
    let signature = curve.sign_ecdsa(&redeem_message, &secret_priv_key);
    let sig_bytes = signature.serialize_der(); 
    let mut sig_bytes_vec = sig_bytes.to_vec();
    sig_bytes_vec.push(elements::SigHashType::All as u8);

    //let sig_bytes_array : [u8; 71] = vec_to_array(sig_bytes_vec); //TODO: how to do this without hammering the array length?
    let script_sig = elements::script::Builder::new()
        .push_slice(sig_bytes_vec.as_slice())
        .push_slice(if is_a_signing==1 {&a_bytes} else {&b_bytes})
        .push_int(is_a_signing) //1 to enter the IF branch where A signed, otherwise 0, to sign with B after locktime
        .push_slice(redeem_script_bytes)
        .into_script();

    //create TxIn  
    let txin = _create_txin(spendable_output, script_sig, locktime_int);
    
    //create TxOut    
    let dest_btc_amt = value_int - fee_int; // 9_999_000; // sat, 0.1BTC, TODO: this should come from parameters along with a fee estimate    
    let dest_btc_txout = _create_txout(a, dest_btc_amt);

    //fee output
    let fee_amount: u64 = fee_int; // 1_000;
    let fee_txout = _create_fee_output(fee_amount);
    
    let raw_tx: Transaction = Transaction {
        version: version,
        lock_time: 1,
        input: vec![txin],
        output: vec![dest_btc_txout.clone(), fee_txout.clone()]
    };

    //Verification step:
    //create script pubkey of the outpoint to be spent for final tx amount verification:
    let p2sh_address = elements::Address::p2sh(&redeem_script, None, &elements::AddressParams::ELEMENTS);
    let input_script_pubkey = p2sh_address.script_pubkey();

    let asset_id = elements::hashes::sha256d::Hash::from_hex("b2e15d0d7a0c94e4e2ce0fe6e8691b9e451377f6e46e8045a86f7c4b5d4f0f23").expect("Valid asset id");
    let spent_txout = TxOut {
        asset: confidential::Asset::Explicit(AssetId::from_slice(&asset_id).unwrap()),
        value: confidential::Value::Explicit(value_int),
        nonce: confidential::Nonce::Null, // unimportant in verification
        script_pubkey: input_script_pubkey,
        witness: elements::TxOutWitness:: default()
    };
    let secp = secp256k1_zkp::Secp256k1::new();
    raw_tx.verify_tx_amt_proofs(&secp, &[spent_txout]).expect("Verification");
    
    return raw_tx.serialize().to_hex();
}

/// Create a Tx with the given script sig spending the given outpoint and paying to a given pubkey
///
/// # Arguments
/// 
/// * `output_to_be_spent` - outpoint to be spent, elements::OutPoint
/// * `value` - string representing an int, value of the spendable output in satoshis, string
/// * `fee` - string representing an int, fee for the transaction in satoshis, string
/// * `script_sig` - script_sig used in this TxIn, it will be the pubKey when creating the dummyTx or the actual script sig with the singed dummytx inside when creating the real tx
/// * `dest_pubkey` - pubkey of the destination of the payment (TODO: differentiate between party A and Party B, B can receive AFTER the timelock)
pub fn _create_tx_to_sign(txin_scriptsig: elements::Script, locktime: i64, output_to_be_spent: elements::OutPoint, value: u64, fee: u64, dest_pubkey: elements::bitcoin::PublicKey, version: u32) -> elements::Transaction {

    //create TxIn  
    let txin = _create_txin(output_to_be_spent, txin_scriptsig, locktime);

    //create main TxOut
    let dest_btc_amt: u64 = value-fee;
    let dest_btc_txout = _create_txout(dest_pubkey, dest_btc_amt);

    //create fee
    let fee_amount: u64 = fee;
    let fee_txout = _create_fee_output(fee_amount);

    let raw_tx: Transaction = Transaction {
        version: version,
        lock_time: 1,
        input: vec![txin],
        output: vec![dest_btc_txout.clone(), fee_txout.clone()]
    };

    return raw_tx;    

}

/// Create a fee TxOut
///
/// # Arguments
/// 
/// * `amount` - fee value, int u64
pub fn _create_fee_output(amount: u64) -> TxOut {
    let asset_id = elements::hashes::sha256d::Hash::from_hex("b2e15d0d7a0c94e4e2ce0fe6e8691b9e451377f6e46e8045a86f7c4b5d4f0f23").expect("Valid asset id");
    return TxOut::new_fee(amount, AssetId::from_slice(&asset_id).unwrap());
}

/// Create a TxOut with the given asset, destination pubkey and amount
///
/// # Arguments
/// 
/// * `pubkey` - pubkey where we will send the coins, elements::bitcoin::PublicKey
/// * `amount` -  value, int u64
pub fn _create_txout(pubkey: elements::bitcoin::PublicKey, amount: u64) -> elements::TxOut {
    let dest_btc_wpkh = elements::WPubkeyHash::from_hash(Some(pubkey.wpubkey_hash()).unwrap().unwrap().as_hash());
    let asset_id = elements::hashes::sha256d::Hash::from_hex("b2e15d0d7a0c94e4e2ce0fe6e8691b9e451377f6e46e8045a86f7c4b5d4f0f23").expect("Valid asset id");
    return TxOut {
        asset: confidential::Asset::Explicit(AssetId::from_slice(&asset_id).unwrap()),
        value: confidential::Value::Explicit(amount),
        //nonce: confidential::Nonce::new_confidential(&mut rng, &secp, &dest_blind_pk).0, //TODO: can I just have all non confidential?
        nonce: confidential::Nonce::default(),
        script_pubkey: elements::Script::new_v0_wpkh(&dest_btc_wpkh),
        witness: elements::TxOutWitness::default(), //questo li aggiusta da solo? - queto witness è come in bitcoin o è specifico per blinded stuff?
    };
}

/// Create a TxIn with the given script sig spending the given outpoint
///
/// # Arguments
/// 
/// * `output_to_be_spent` - outpoint to be spent, elements::OutPoint
/// * `script_sig` - script_sig used in this TxIn, it will be the pubKey when creating the dummyTx or the actual script sig with the singed dummytx inside when creating the real tx
/// * `locktime` - locktime to put into nsequence, must be converted to u32, i64
pub fn _create_txin(output_to_be_spent: elements::OutPoint, script_sig: elements::Script, locktime: i64) -> elements::TxIn {
    let locktime_u32 = locktime as u32;
    return elements::TxIn{
        previous_output: output_to_be_spent,
        is_pegin: false,
        has_issuance: false,
        script_sig: script_sig,
        sequence:locktime_u32,
        asset_issuance: elements::AssetIssuance::default(),
        witness: elements::TxInWitness::default()
    };
}


/// Get the keys and the lock time to create the redeem script and return it
///
/// # Arguments
///
/// * `pka` - PublicKey of party A, the one that can spend at any time, elements::bitcoin::PublicKey
/// * `pkb` - PublicKey of party B, the one that can spend after the timelock, elements::bitcoin::PublicKey
/// * `locktime` - i64, the number of blocks after which keyB can spend, int
pub fn _create_redeem_script<'a>(pka: &elements::bitcoin::PublicKey, pkb: &elements::bitcoin::PublicKey, locktime_int: i64) -> elements::Script{

    let pka_sha160 = pka.pubkey_hash();
    let pkb_sha160 = pkb.pubkey_hash();

    let locktime_bytes = locktime_int.to_be_bytes();
    let mut padded_locktime_bytes = [0; 8];
    padded_locktime_bytes[..locktime_bytes.len()].copy_from_slice(&locktime_bytes);

    let redeem_script = elements::script::Builder::new()
        .push_opcode(elements::opcodes::all::OP_IF)
        .push_opcode(elements::opcodes::all::OP_DUP)
        .push_opcode(elements::opcodes::all::OP_HASH160)
        .push_slice(&pka_sha160)
        .push_opcode(elements::opcodes::all::OP_ELSE)
        .push_int(locktime_int)
        .push_opcode(elements::opcodes::all::OP_CSV)
        .push_opcode(elements::opcodes::all::OP_DROP)
        .push_opcode(elements::opcodes::all::OP_DUP)
        .push_opcode(elements::opcodes::all::OP_HASH160)
        .push_slice(&pkb_sha160)
        .push_opcode(elements::opcodes::all::OP_ENDIF)
        .push_opcode(elements::opcodes::all::OP_EQUALVERIFY)
        .push_opcode(elements::opcodes::all::OP_CHECKSIG)
        .into_script();

    return redeem_script;
}

pub fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError> {
    (0..s.len())
        .step_by(2)
        .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
        .collect()
}

pub fn vec_to_array<T, const N: usize>(v: Vec<T>) -> [T; N] {
    v.try_into()
        .unwrap_or_else(|v: Vec<T>| panic!("Expected a Vec of length {} but it was {}", N, v.len()))
}